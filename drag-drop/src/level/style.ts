import styled from 'styled-components';

const LevelWrapper = styled.div`
  width: 100%;
  height: 100%;
  background-color: #e6e6e6;
  border-radius: 1em;
`;

export {LevelWrapper};
