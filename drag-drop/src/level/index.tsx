import React from 'react';
import {Card} from '../card';
import {LevelWrapper} from './style';


interface ILevel {
  title: string;
  value: string;
  tasks: any[];
  color: string;
  onDragOver: (event: any) => void;
  onDrop: (event: any) => void;
}

const Level = ({title, value, tasks, onDragOver, onDrop, color}: ILevel) => {
  const onDragStart = (event: any, taskName: string) => {
    event.dataTransfer.setData('taskName', taskName);
  };

  return (
    <LevelWrapper onDragOver={onDragOver} onDrop={onDrop}>
      <span className="group-header">{title}</span>
      {tasks.map(task => {
        if (task.type === value)
          return (
            <Card
              type={task.type}
              name={task.name}
              color={color}
              onDragStart={(event: any) => onDragStart(event, task.name)}
            />
          );
      })}
    </LevelWrapper>
  );
};

export {Level};
