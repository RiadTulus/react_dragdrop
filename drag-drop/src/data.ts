export const myLevels = [
    {name:'In Progress', value:'inProgress', color:'black'},
    {name:'To Test', value:'toTest', color:'orange'},
    {name: 'To Do', value:'toDo', color:'grey'},
    {name: 'Done', value:'Done', color:'green'},
]


export const mytasks = {
    tasks: [
      {
        name: 'Read book',
        type: 'inProgress',
        color: 'white',
      },
      {
        name: 'Pay bills',
        type: 'toTest',
        color: 'white',
      },
      {
        name: 'Go to the gym',
        type: 'Done',
        color: 'white',
      },
      {
        name: 'Play',
        type: 'toDo',
        color: 'white',
      },
      {
        name: 'Play ball',
        type: 'toDo',
        color: 'white',
      },
      {
        name: 'Play baseball',
        type: 'toDo',
        color: 'white',
      },
    ],
  };