import React from 'react';
import { TaskWrapper, TaskOwner } from './style';

interface ICard {
  name: string;
  color: string;
  type: string;
  onDragStart: (event: any) => void;
}

const Card = ({name, color, onDragStart}: ICard) => {
  return (
    <TaskWrapper
      onDragStart={onDragStart}
      draggable
      color={color}
    >
      <TaskOwner>RB</TaskOwner>
      {name}
    </TaskWrapper>
  );
};

export {Card};
