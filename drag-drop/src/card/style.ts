import styled from 'styled-components';

const TaskWrapper = styled.div`
  width: 90%;
  height: 6rem;
  border-radius: 0.2em;
  border-left-style: solid;
  border-left-width: 0.4rem;
  border-left-color: ${props => props.color};
  box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.75);
  background-color: yellow;
  margin: 10px auto;
  display: flex;
  text-align: center; 
  background-color: white;

  &:hover {
    cursor: pointer;
  }
`;

const TaskOwner = styled.div`
    height: 30px;
    width: 30px;
    border-radius: 50%;
    position: relative;
    bottom:-60%;
    left: 5%;
    background-color: green;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 15px;
    color: white;
`;

export {TaskWrapper, TaskOwner};
