import React, {useState} from 'react';
import './App.css';
import {Card} from './card';
import {Level} from './level';
import {myLevels, mytasks} from './data'

const App: React.FC = () => {
  const [myTasks, setMyTasks] = useState(mytasks);

  const onDragOver = (event: any) => {
    event.preventDefault();
  };

  const onDrop = (event: any, cat: string) => {
    let taskName = event.dataTransfer.getData('taskName');

    let tasks = myTasks.tasks.filter(task => {
      if (task.name == taskName) {
        task.type = cat;
      }
      return task;
    });

    setMyTasks({
      ...myTasks,
      tasks,
    });
  };

  return (
    <div className="drag-container">
      <h2 className="head">Task Dashbord</h2>
      <div className="levels-container">
        <div className="level-container">
          {
            myLevels.map(level => (
              <Level
              title={level.name}
              value={level.value}
              onDragOver={(event: any) => onDragOver(event)}
              onDrop={event => onDrop(event, level.value)}
              tasks={myTasks.tasks}
              color={level.color}
            />
            ))
          }
        </div>
      </div>
    </div>
  );
};

export default App;
